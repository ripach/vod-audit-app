package de.jvts.vod.domainobject;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.google.common.base.MoreObjects;

import de.jvts.vod.domainvalue.AuditPeriod;
import de.jvts.vod.domainvalue.AuditState;
import de.jvts.vod.domainvalue.Carrier;

@Entity
public class Audit {

    @Id @GeneratedValue private Long id;
    @ManyToOne(optional = false, cascade = CascadeType.MERGE)
    private Carrier issuingCarrier;
    @ManyToOne(optional = false, cascade = CascadeType.MERGE)
    private Carrier flownCarrier;
    @Embedded private AuditPeriod period;
    @Embedded private AuditState state;
    private LocalDate creationDate;
    private LocalDate finalizationDate;
    private int totalVolume;

    protected Audit() {
        // default constructor for hibernate
    }

    public Audit(final Carrier issuingCarrier, final Carrier flownCarrier, final AuditPeriod period, final AuditState state,
            final LocalDate creationDate, final LocalDate finalizationDate, final int totalVolume) {
        this.issuingCarrier = issuingCarrier;
        this.flownCarrier = flownCarrier;
        this.period = period;
        this.state = state;
        this.creationDate = creationDate;
        this.finalizationDate = finalizationDate;
        this.totalVolume = totalVolume;
    }

    public Carrier getIssuingCarrier() {
        return this.issuingCarrier;
    }

    public void setIssuingCarrier(final Carrier issuingCarrier) {
        this.issuingCarrier = issuingCarrier;
    }

    public Carrier getFlownCarrier() {
        return this.flownCarrier;
    }

    public void setFlownCarrier(final Carrier flownCarrier) {
        this.flownCarrier = flownCarrier;
    }

    public AuditPeriod getPeriod() {
        return this.period;
    }

    public void setPeriod(final AuditPeriod period) {
        this.period = period;
    }

    public AuditState getState() {
        return this.state;
    }

    public void setState(final AuditState state) {
        this.state = state;
    }

    public Long getId() {
        return this.id;
    }

    public LocalDate getCreationDate() {
        return this.creationDate;
    }

    public LocalDate getFinalizationDate() {
        return this.finalizationDate;
    }

    public int getTotalVolume() {
        return this.totalVolume;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("issuingCarrier", this.issuingCarrier).add("flownCarrier", this.flownCarrier)
                .add("period", this.period).add("state", this.state).add("creationDate", this.creationDate).add("finalizationDate", this.finalizationDate)
                .add("id", this.id).toString();
    }

}
