package de.jvts.vod.domainobject;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.google.common.base.MoreObjects;

@Entity
public class Coupon {

	@Id @GeneratedValue Long id;

	String issueingCarrier;
	String flownCarrier;
	LocalDate transactionDate;

	BigDecimal fareValue;
	BigDecimal yqValue;

	protected Coupon() {
		// default constructor for hibernate
	}

	public Coupon(String issueingCarrier, String flownCarrier, LocalDate transactionDate, BigDecimal fareValue,
			BigDecimal yqValue) {
		super();
		this.issueingCarrier = issueingCarrier;
		this.flownCarrier = flownCarrier;
		this.transactionDate = transactionDate;
		this.fareValue = fareValue;
		this.yqValue = yqValue;
	}

	public String getIssueingCarrier() {
		return issueingCarrier;
	}

	public void setIssueingCarrier(String issueingCarrier) {
		this.issueingCarrier = issueingCarrier;
	}

	public String getFlownCarrier() {
		return flownCarrier;
	}

	public void setFlownCarrier(String flownCarrier) {
		this.flownCarrier = flownCarrier;
	}

	public LocalDate getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(LocalDate transactionDate) {
		this.transactionDate = transactionDate;
	}

	public BigDecimal getFareValue() {
		return fareValue;
	}

	public void setFareValue(BigDecimal fareValue) {
		this.fareValue = fareValue;
	}

	public BigDecimal getYqValue() {
		return yqValue;
	}

	public void setYqValue(BigDecimal yqValue) {
		this.yqValue = yqValue;
	}

	public Long getId() {
		return id;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("issueingCarrier", issueingCarrier)
				.add("flownCarrier", flownCarrier).add("transactionDate", transactionDate).add("fareValue", fareValue)
				.add("yqValue", yqValue).toString();
	}
}
