package de.jvts.vod.ui;

import java.text.MessageFormat;
import java.util.ResourceBundle;

public final class I18n {
    private I18n() {
    }

    private static ResourceBundle bundle;

    private static ResourceBundle getBundle() {
        if (bundle == null) {
            bundle = ResourceBundle.getBundle("vod-audit");
        }
        return bundle;
    }

    public static String getMessage(final String key) {
        return getBundle().getString(key);
    }

    public static String getMessage(final String key, final Object... arguments) {
        return MessageFormat.format(getMessage(key), arguments);
    }

    public static String getMessage(final Enum<?> enumVal, final Object... arguments) {
        return MessageFormat.format(getMessage(enumVal.toString().toLowerCase()), arguments);
    }

}