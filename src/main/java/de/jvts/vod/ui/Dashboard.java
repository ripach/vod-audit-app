package de.jvts.vod.ui;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

@SpringComponent
@UIScope
public class Dashboard {

    private final VerticalLayout layout;

    private final Panel activeAuditPanel;
    private final Panel finishedAuditPanel;

    private final ActiveAuditView activeAuditView;
    private final FinishedAuditView finishedAuditView;

    @Autowired
    public Dashboard(final ActiveAuditView activeAuditView, final FinishedAuditView finishedAuditView) {
        this.activeAuditView = activeAuditView;
        this.finishedAuditView = finishedAuditView;

        this.finishedAuditPanel = new Panel("Finished Audits", finishedAuditView);
        this.finishedAuditPanel.addStyleName(ValoTheme.PANEL_BORDERLESS);

        this.activeAuditPanel = new Panel("Active Audits", activeAuditView);
        this.activeAuditPanel.addStyleName(ValoTheme.PANEL_BORDERLESS);
        this.layout = new VerticalLayout(this.activeAuditPanel, this.finishedAuditPanel);
        this.layout.addStyleName("dashboard");
    }

    public VerticalLayout getLayout() {
        return this.layout;
    }

    public void refresh() {
        this.activeAuditView.refresh();
        this.finishedAuditView.refresh();
    }

}
