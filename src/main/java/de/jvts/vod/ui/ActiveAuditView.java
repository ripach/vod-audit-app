package de.jvts.vod.ui;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import de.jvts.vod.persistence.AuditRepository;

@SpringComponent
@UIScope
public class ActiveAuditView extends VerticalLayout {

    private String carrierCode = "LH";
    private final HorizontalLayout horizontalLayout;
    private VerticalLayout ownAuditsLayout;
    private VerticalLayout partnerAuditsLayout;

    private final AuditRepository auditRepo;

    /**
     *
     */
    private static final long serialVersionUID = 1115961891000779268L;

    @Autowired
    public ActiveAuditView(final AuditRepository auditRepo) {
        this.auditRepo = auditRepo;

        final ActiveAuditFilterView filterView = new ActiveAuditFilterView();
        this.ownAuditsLayout = new AuditListView("Own", auditRepo.findActiveOwnAudits(this.carrierCode), this.carrierCode);
        this.partnerAuditsLayout = new AuditListView("Partner", auditRepo.findActivePartnerAudits(this.carrierCode),
                this.carrierCode);

        addStyleName("activeAuditLayout");
        this.setWidth(100, Unit.PERCENTAGE);

        this.horizontalLayout = new HorizontalLayout(this.ownAuditsLayout, this.partnerAuditsLayout);
        this.horizontalLayout.setWidth(100, Unit.PERCENTAGE);
        addComponents(filterView, this.horizontalLayout);
    }

    public void refresh() {
        this.carrierCode = UI.getCurrent().getSession().getAttribute("carrierCode").toString();
        this.ownAuditsLayout = new AuditListView("Own", this.auditRepo.findActiveOwnAudits(this.carrierCode), this.carrierCode);
        this.partnerAuditsLayout = new AuditListView("Partner", this.auditRepo.findActivePartnerAudits(this.carrierCode),
                this.carrierCode);
        this.horizontalLayout.removeAllComponents();
        this.horizontalLayout.addComponents(this.ownAuditsLayout, this.partnerAuditsLayout);
    }

}
