package de.jvts.vod.ui;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.data.Binder;
import com.vaadin.data.converter.StringToBigDecimalConverter;
import com.vaadin.event.ShortcutAction;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.DateField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.ValoTheme;

import de.jvts.vod.domainobject.Coupon;
import de.jvts.vod.persistence.CouponRepository;

@SpringComponent
@UIScope
public class CouponEditor extends Window {

	private static final long serialVersionUID = 8280623928880267083L;

	VerticalLayout layout;

	private final CouponRepository repository;

	/**
	 * The currently edited customer
	 */
	private Coupon coupon;

	/* Fields to edit properties in Customer entity */
	TextField issueingCarrier = new TextField("Issueing Carrier");
	TextField flownCarrier = new TextField("Flown Carrier");
	DateField transactionDate = new DateField("Transaction Date");

	TextField fareValue = new TextField("Fare Value");
	TextField yqValue = new TextField("YQ Value");

	/* Action buttons */
	Button save = new Button("Save", VaadinIcons.CHECK_CIRCLE_O);
	Button cancel = new Button("Cancel");
	Button delete = new Button("Delete", VaadinIcons.TRASH);
	CssLayout actions = new CssLayout(save, cancel, delete);

	Binder<Coupon> binder = new Binder<>(Coupon.class);

	@Autowired
	public CouponEditor(CouponRepository repository) {
		super("Edit Coupon");
		this.repository = repository;

		layout = new VerticalLayout(issueingCarrier, flownCarrier, transactionDate, fareValue, yqValue, actions);
		setContent(layout);

		// bind using naming convention
		binder.forField(issueingCarrier).bind(Coupon::getIssueingCarrier, Coupon::setIssueingCarrier);
		binder.forField(flownCarrier).bind(Coupon::getFlownCarrier, Coupon::setFlownCarrier);
		binder.forField(transactionDate).bind(Coupon::getTransactionDate, Coupon::setTransactionDate);
		binder.forField(fareValue).withConverter(new StringToBigDecimalConverter("Unable to ")).bind("fareValue");
		binder.forField(yqValue).withConverter(new StringToBigDecimalConverter("Unable to ")).bind("yqValue");
		// binder.bind(fareValue, "fareValue");
		// binder.bind(yqValue, "yqValue");

		// Configure and style components
		layout.setSpacing(true);
		actions.setStyleName(ValoTheme.LAYOUT_COMPONENT_GROUP);
		save.setStyleName(ValoTheme.BUTTON_PRIMARY);
		save.setClickShortcut(ShortcutAction.KeyCode.ENTER);

		// wire action buttons to save, delete and reset
		save.addClickListener(e -> repository.save(coupon));
		delete.addClickListener(e -> repository.delete(coupon));
		cancel.addClickListener(e -> setVisible(false));
	}

	public interface ChangeHandler {

		void onChange();
	}

	public final void editCoupon(Coupon c) {
		if (c == null) {
			setVisible(false);
			return;
		}
		final boolean persisted = c.getId() != null;
		if (persisted) {
			// Find fresh entity for editing
			coupon = repository.findOne(c.getId());
		} else {
			coupon = c;
		}
		cancel.setVisible(persisted);

		// Bind customer properties to similarly named fields
		// Could also use annotation or "manual binding" or programmatically
		// moving values from fields to entities before saving
		binder.setBean(coupon);

		// A hack to ensure the whole form is visible
		save.focus();
		// Select all text in firstName field automatically
		issueingCarrier.selectAll();
		center();
		setVisible(true);
		setClosable(false);
	}

	public void setChangeHandler(ChangeHandler h) {
		// ChangeHandler is notified when either save or delete
		// is clicked
		save.addClickListener(e -> h.onChange());
		delete.addClickListener(e -> h.onChange());
	}

}
