package de.jvts.vod.ui;

import java.util.List;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

import de.jvts.vod.domainobject.Audit;

public class AuditListView extends VerticalLayout {

    private static final long serialVersionUID = 2356161842882854416L;

    public AuditListView(final String caption, final List<Audit> audits, final String ownCarrierCode) {
        super();
        final Label captionLabel = new Label(caption);
        captionLabel.addStyleName("auditListView-captionLabel");
        this.addComponent(captionLabel);
        setComponentAlignment(captionLabel, Alignment.TOP_CENTER);
        for (final Audit audit : audits) {
            this.addComponent(new AuditView(audit, ownCarrierCode));
            this.setWidth(100, Unit.PERCENTAGE);
        }
    }

}
