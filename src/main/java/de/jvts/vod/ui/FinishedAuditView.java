package de.jvts.vod.ui;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Grid;
import com.vaadin.ui.UI;

import de.jvts.vod.domainobject.Audit;
import de.jvts.vod.persistence.AuditRepository;

@SpringComponent
@UIScope
public class FinishedAuditView extends Grid<Audit> {

    private String carrierCode = "LH";
    private final AuditRepository auditRepo;
    /**
     *
     */
    private static final long serialVersionUID = 1670176976745711248L;

    @Autowired
    public FinishedAuditView(final AuditRepository auditRepo) {
        super();
        this.auditRepo = auditRepo;
        this.setWidth(100, Unit.PERCENTAGE);
        this.setItems(auditRepo.findFinishedAudits());
        this.addColumn(Audit::getPeriod).setCaption("Audit Period");
        this.addColumn(this::getPartnerCarrierName).setCaption("Partner");
        this.addColumn((a) -> a.getIssuingCarrier().getTwoLetterCode()).setCaption("Stock");
        this.addColumn(Audit::getCreationDate).setCaption("Create Date");
        this.addColumn(Audit::getFinalizationDate).setCaption("Finalization Date");
        this.addColumn(Audit::getTotalVolume).setCaption("Total Volume");
    }

    private String getPartnerCarrierName(final Audit audit) {
        return (this.carrierCode.equals(audit.getIssuingCarrier().getTwoLetterCode()) ? audit.getFlownCarrier().getCarrierName()
                : audit.getIssuingCarrier().getCarrierName());
    }

    public void refresh() {
        this.carrierCode = UI.getCurrent().getSession().getAttribute("carrierCode").toString();
        this.setItems(this.auditRepo.findFinishedAudits());
    }

}
