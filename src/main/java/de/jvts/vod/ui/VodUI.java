package de.jvts.vod.ui;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SpringUI
@Theme("valo")
public class VodUI extends UI {

    private static final long serialVersionUID = -9074812835703903674L;

    private final Dashboard dashboard;
    private VerticalLayout layout;

    @Autowired
    public VodUI(final Dashboard dashboard) {
        this.dashboard = dashboard;
    }

    @Override
    protected void init(final VaadinRequest request) {
        setTheme("vodtheme");

        final ComboBox<String> carrierComboBox = new ComboBox<>();
        carrierComboBox.setItems(Arrays.asList("LH", "LX"));
        carrierComboBox.setEmptySelectionAllowed(false);
        carrierComboBox.setTextInputAllowed(false);
        carrierComboBox.setSelectedItem("LH");
        final Label label = new Label("VOD Audit App");
        final HorizontalLayout headline = new HorizontalLayout(label, carrierComboBox);
        headline.addStyleName("app-headline");
        headline.setWidth(100, Unit.PERCENTAGE);
        headline.setComponentAlignment(label, Alignment.MIDDLE_LEFT);
        headline.setComponentAlignment(carrierComboBox, Alignment.MIDDLE_RIGHT);
        carrierComboBox.addSelectionListener(e -> {
            UI.getCurrent().getSession().setAttribute("carrierCode", carrierComboBox.getValue());
            this.dashboard.refresh();
        });

        this.layout = new VerticalLayout(headline, this.dashboard.getLayout());
        this.layout.setMargin(false);
        this.layout.addStyleName("app-content");
        setContent(this.layout);
    }

    // private final CouponEditor editor;
    // CouponRepository couponRepo;
    // Grid<Coupon> grid;
    // final TextField filter;
    // private final Button addNewBtn;
    //
    // @Autowired
    // public VodUI(CouponRepository couponRepo, CouponEditor editor) {
    // this.couponRepo = couponRepo;
    // this.grid = new Grid<>(Coupon.class);
    // this.filter = new TextField();
    // this.editor = editor;
    // this.addNewBtn = new Button("New Coupon", FontAwesome.PLUS);
    // }
    //
    // @Override
    // protected void init(VaadinRequest request) {
    // // build layout
    // HorizontalLayout actions = new HorizontalLayout(filter, addNewBtn);
    // VerticalLayout mainLayout = new VerticalLayout(actions, grid);
    // setContent(mainLayout);
    //
    // grid.setWidth(95, Unit.PERCENTAGE);
    // grid.setHeight(300, Unit.PIXELS);
    // grid.setColumns("id", "issueingCarrier", "flownCarrier",
    // "transactionDate", "fareValue", "yqValue");
    //
    // filter.setPlaceholder("Filter by issueingCarrier");
    //
    // // Hook logic to components
    //
    // // Replace listing with filtered content when user changes filter
    // filter.setValueChangeMode(ValueChangeMode.LAZY);
    // filter.addValueChangeListener(e -> listCoupons(e.getValue()));
    //
    // addWindow(editor);
    // editor.setVisible(false);
    //
    // // Connect selected Customer to editor or hide if none is selected
    // grid.asSingleSelect().addValueChangeListener(e -> {
    // editor.editCoupon(e.getValue());
    // });
    //
    // // Instantiate and edit new Customer the new button is clicked
    // addNewBtn.addClickListener(e -> {
    // editor.editCoupon(new Coupon("", "", null, BigDecimal.ZERO,
    // BigDecimal.ZERO));
    // });
    //
    // // Listen changes made by the editor, refresh data from backend
    // editor.setChangeHandler(() -> {
    // editor.setVisible(false);
    // listCoupons(filter.getValue());
    // });
    //
    // // Initialize listing
    // listCoupons(null);
    // }
    //
    // private void listCoupons(String carrier) {
    // grid.setItems(couponRepo.findByFlownCarrier(carrier));
    // }

}
