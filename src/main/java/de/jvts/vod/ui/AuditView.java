package de.jvts.vod.ui;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

import de.jvts.vod.domainobject.Audit;
import de.jvts.vod.domainvalue.AuditStateCarrierAssignment;

public class AuditView extends GridLayout {

    private static final Logger log = LoggerFactory.getLogger(AuditView.class);

    private static final long serialVersionUID = 7979663645600201028L;

    public AuditView(final Audit audit, final String ownCarrierCode) {
        super(3, 3);
        log.debug("Creating AuditView for Audit: {}", audit);

        setColumnExpandRatio(0, 0.2f);
        setColumnExpandRatio(1, 0.6f);
        setColumnExpandRatio(2, 0.2f);
        this.setWidth(100, Unit.PERCENTAGE);
        this.setHeight(120, Unit.PIXELS);
        setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);

        final Label auditPeriodLabel = new Label(audit.getPeriod().toString());
        auditPeriodLabel.addStyleName("auditView-periodLabel");
        final VerticalLayout vl = new VerticalLayout(auditPeriodLabel);
        vl.setWidth(100, Unit.PERCENTAGE);
        vl.setSpacing(false);
        this.addComponent(vl, 0, 0, 0, 2);

        final String partnerCarrier = (ownCarrierCode.equals(audit.getIssuingCarrier().getTwoLetterCode()) ? audit.getFlownCarrier().getCarrierName()
                : audit.getIssuingCarrier().getCarrierName());
        final Label partnerCarrierLabel = new Label(partnerCarrier);
        partnerCarrierLabel.addStyleName("auditView-headline");
        final VerticalLayout vl2 = new VerticalLayout(partnerCarrierLabel);
        vl2.setSpacing(false);
        vl2.setWidth(100, Unit.PERCENTAGE);
        this.addComponent(partnerCarrierLabel, 1, 0);

        final Label issuingCarrierLabel = new Label(audit.getIssuingCarrier().getTwoLetterCode() + "-Stock");
        this.addComponent(issuingCarrierLabel, 1, 1);

        final Label activityLabel = new Label(I18n.getMessage(audit.getState().getActivity()));
        this.addComponent(activityLabel, 1, 2);

        final Button goButton = new Button("Go");
        final VerticalLayout vl3 = new VerticalLayout();
        if (audit.getState().getCarrierAssignment().equals(AuditStateCarrierAssignment.FLOWN)
                && audit.getFlownCarrier().getTwoLetterCode().equals(ownCarrierCode)
                || audit.getState().getCarrierAssignment().equals(AuditStateCarrierAssignment.ISSUING)
                && audit.getIssuingCarrier().getTwoLetterCode().equals(ownCarrierCode)) {
            vl3.addComponent(goButton);
        }
        vl3.setSpacing(false);
        vl3.setWidth(100, Unit.PERCENTAGE);
        this.addComponent(vl3, 2, 0, 2, 2);
        setComponentAlignment(vl3, Alignment.MIDDLE_RIGHT);
    }

}
