package de.jvts.vod.ui;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.themes.ValoTheme;

public class ActiveAuditFilterView extends HorizontalLayout {

    /**
     *
     */
    private static final long serialVersionUID = -6491715541513413284L;

    private final HorizontalLayout filter;
    private final Label filterLabel;

    public ActiveAuditFilterView() {
        this.filterLabel = new Label("Filter");
        addStyleName("activeAuditFilterView");
        final Button expandButton = new Button(VaadinIcons.PLUS_CIRCLE);
        expandButton.addStyleName(ValoTheme.BUTTON_LINK);
        expandButton.addClickListener(this::showFilters);
        setDefaultComponentAlignment(Alignment.MIDDLE_RIGHT);
        setWidth(100, Unit.PERCENTAGE);
        final Label l = new Label("");
        addComponents(l, this.filterLabel, expandButton);
        setExpandRatio(l, 1f);

        final Button closeFilterButton = new Button("Close Filter >");
        closeFilterButton.addStyleName(ValoTheme.BUTTON_LINK);
        closeFilterButton.addClickListener(this::hideFilters);

        this.filter = new HorizontalLayout(new Label("Activity"), new Label("Partner"), new Label("Audit Layout"), closeFilterButton);
        this.filter.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
    }

    private void showFilters(final ClickEvent ce) {
        removeComponent(this.filterLabel);
        addComponentAsFirst(this.filter);
    }

    private void hideFilters(final ClickEvent ce) {
        removeComponent(this.filter);
        addComponent(this.filterLabel, 1);
    }
}
