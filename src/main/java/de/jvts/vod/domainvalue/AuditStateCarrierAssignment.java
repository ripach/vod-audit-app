package de.jvts.vod.domainvalue;

public enum AuditStateCarrierAssignment {
	ISSUING, FLOWN, NONE
}
