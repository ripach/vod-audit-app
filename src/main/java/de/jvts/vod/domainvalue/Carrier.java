package de.jvts.vod.domainvalue;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Carrier {

    @Id
    @GeneratedValue
    private Long id;

    String twoLetterCode;
    String carrierName;

    protected Carrier() {
        // default constructor for hibernate
    }

    public Carrier(final String twoLetterCode, final String carrierName) {
        super();
        this.twoLetterCode = twoLetterCode;
        this.carrierName = carrierName;
    }

    public String getTwoLetterCode() {
        return this.twoLetterCode;
    }

    public String getCarrierName() {
        return this.carrierName;
    }

}
