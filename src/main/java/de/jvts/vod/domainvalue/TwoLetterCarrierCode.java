package de.jvts.vod.domainvalue;

public class TwoLetterCarrierCode {

    String value;

    protected TwoLetterCarrierCode(final String value) {
        this.value = value;
    }

    public static TwoLetterCarrierCode of(final String value) {
        return new TwoLetterCarrierCode(value);
    }

    @Override
    public String toString() {
        return this.value;
    }

}
