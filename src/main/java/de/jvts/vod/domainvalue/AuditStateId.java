package de.jvts.vod.domainvalue;

public enum AuditStateId {

	IMPORTED(AuditStateActivity.IMPORTED, AuditStateCarrierAssignment.NONE),
	FIRST_EVALUATION_ISSUING(AuditStateActivity.FIRST_EVALUATION, AuditStateCarrierAssignment.ISSUING),
	CROSS_CHECK_1_ISSUING(AuditStateActivity.CROSS_CHECK_1, AuditStateCarrierAssignment.ISSUING),
	CROSS_CHECK_2_ISSUING(AuditStateActivity.CROSS_CHECK_2, AuditStateCarrierAssignment.ISSUING),
	CROSS_CHECK_3_ISSUING(AuditStateActivity.CROSS_CHECK_3, AuditStateCarrierAssignment.ISSUING),
	CROSS_CHECK_4_ISSUING(AuditStateActivity.CROSS_CHECK_4, AuditStateCarrierAssignment.ISSUING),
	FIRST_EVALUATION_FLOWN(AuditStateActivity.FIRST_EVALUATION, AuditStateCarrierAssignment.FLOWN),
	CROSS_CHECK_1_FLOWN(AuditStateActivity.CROSS_CHECK_1, AuditStateCarrierAssignment.FLOWN),
	CROSS_CHECK_2_FLOWN(AuditStateActivity.CROSS_CHECK_2, AuditStateCarrierAssignment.FLOWN),
	CROSS_CHECK_3_FLOWN(AuditStateActivity.CROSS_CHECK_3, AuditStateCarrierAssignment.FLOWN),
	CROSS_CHECK_4_FLOWN(AuditStateActivity.CROSS_CHECK_4, AuditStateCarrierAssignment.FLOWN),
	SAMPLE_ANALYSIS(AuditStateActivity.SAMPLE_ANALYSIS, AuditStateCarrierAssignment.ISSUING),
	ROOT_CAUSE_ANALYSIS(AuditStateActivity.ROOT_CAUSE_ANALYSIS, AuditStateCarrierAssignment.ISSUING),
	FINALIZED(AuditStateActivity.FINALIZED, AuditStateCarrierAssignment.NONE);

	private AuditStateActivity activity;
	private AuditStateCarrierAssignment assignedCarrier;

	private AuditStateId(AuditStateActivity activity, AuditStateCarrierAssignment assignedCarrier) {
		this.assignedCarrier = assignedCarrier;
		this.activity = activity;
	}

	public AuditStateCarrierAssignment getAssignedCarrier() {
		return assignedCarrier;
	}

	public AuditStateActivity getActivity() {
		return activity;
	}

}
