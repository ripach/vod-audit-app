package de.jvts.vod.domainvalue;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.google.common.base.MoreObjects;

@Embeddable
public class AuditState {

	@Enumerated(EnumType.STRING) AuditStateId stateId;

	@Enumerated(EnumType.STRING) private AuditStateActivity activity;
	@Enumerated(EnumType.STRING) private AuditStateCarrierAssignment carrierAssignment;

	protected AuditState() {
		// default constructor for hibernate
	}

	public AuditState(AuditStateId stateId) {
		this.stateId = stateId;
		this.activity = stateId.getActivity();
		this.carrierAssignment = stateId.getAssignedCarrier();
	}

	public AuditStateId getStateId() {
		return stateId;
	}

	public void setStateId(AuditStateId stateId) {
		this.stateId = stateId;
		this.activity = stateId.getActivity();
		this.carrierAssignment = stateId.getAssignedCarrier();
	}

	public AuditStateActivity getActivity() {
		return activity;
	}

	public AuditStateCarrierAssignment getCarrierAssignment() {
		return carrierAssignment;
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this).add("stateId", stateId).toString();
	}

}
