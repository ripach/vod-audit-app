package de.jvts.vod.domainvalue;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.persistence.Embeddable;

@Embeddable
public class AuditPeriod {

	private static final DateTimeFormatter MONTH_FORMATTER = DateTimeFormatter.ofPattern("MM/yy");

	private LocalDate fromDate;
	private LocalDate toDate;

	protected AuditPeriod() {
		// default constructor for hibernate
	}

	public AuditPeriod(LocalDate fromDate, LocalDate toDate) {
		this.fromDate = fromDate;
		this.toDate = toDate;
	}

	public AuditPeriod(LocalDate of) {
		this(of.withDayOfMonth(1), of.withDayOfMonth(1).plusMonths(1).minusDays(1));
	}

	@Override
	public String toString() {
		if (fromDate.getYear() == toDate.getYear() && fromDate.getMonth().equals(toDate.getMonth())) {
			return fromDate.format(MONTH_FORMATTER);
		} else {
			return String.format("%s - %s", fromDate.format(MONTH_FORMATTER), toDate.format(MONTH_FORMATTER));
		}
	}

	public LocalDate getFromDate() {
		return fromDate;
	}

	public void setFromDate(LocalDate fromDate) {
		this.fromDate = fromDate;
	}

	public LocalDate getToDate() {
		return toDate;
	}

	public void setToDate(LocalDate toDate) {
		this.toDate = toDate;
	}

}
