package de.jvts.vod;

import java.time.LocalDate;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import de.jvts.vod.domainobject.Audit;
import de.jvts.vod.domainvalue.AuditPeriod;
import de.jvts.vod.domainvalue.AuditState;
import de.jvts.vod.domainvalue.AuditStateId;
import de.jvts.vod.domainvalue.Carrier;
import de.jvts.vod.persistence.AuditRepository;
import de.jvts.vod.persistence.CarrierRepository;

@SpringBootApplication
public class App {

    private static final Logger log = LoggerFactory.getLogger(App.class);

    public static void main(final String[] args) {
        SpringApplication.run(App.class);
    }

    @Bean
    public CommandLineRunner loadData(final CarrierRepository carrierRepo, final AuditRepository auditRepo) {
        return (args) -> {
            final AuditPeriod p1 = new AuditPeriod(LocalDate.of(2016, 11, 1));
            final AuditPeriod p2 = new AuditPeriod(LocalDate.of(2016, 10, 1));
            final AuditPeriod p3 = new AuditPeriod(LocalDate.of(2016, 11, 1), LocalDate.of(2016, 12, 31));
            final AuditPeriod p9 = new AuditPeriod(LocalDate.of(2016, 9, 1));
            final AuditPeriod p8 = new AuditPeriod(LocalDate.of(2016, 8, 1), LocalDate.of(2016, 9, 30));

            final Carrier LH = new Carrier("LH", "Lufthansa");
            final Carrier LX = new Carrier("LX", "Swiss");
            final Carrier OS = new Carrier("OS", "Austrian");
            final Carrier UA = new Carrier("UA", "United Airlines");

            carrierRepo.save(Arrays.asList(LH, LX, OS, UA));

            final Audit a1 = new Audit(LH, UA, p1, new AuditState(AuditStateId.CROSS_CHECK_2_ISSUING), LocalDate.now(), null, 100);
            final Audit a2 = new Audit(LH, OS, p2, new AuditState(AuditStateId.ROOT_CAUSE_ANALYSIS), LocalDate.now(), null, 100);
            final Audit a3 = new Audit(LX, LH, p3, new AuditState(AuditStateId.CROSS_CHECK_2_FLOWN), LocalDate.now(), null, 100);
            final Audit a4 = new Audit(LH, LX, p3, new AuditState(AuditStateId.FIRST_EVALUATION_FLOWN), LocalDate.now(), null, 100);
            final Audit a5 = new Audit(UA, LH, p1, new AuditState(AuditStateId.CROSS_CHECK_4_ISSUING), LocalDate.now(), null, 100);

            final Audit f1 = new Audit(LH, UA, p2, new AuditState(AuditStateId.FINALIZED), LocalDate.now(), LocalDate.of(2017, 2, 19), 100);
            final Audit f2 = new Audit(UA, LH, p2, new AuditState(AuditStateId.FINALIZED), LocalDate.now(), LocalDate.of(2017, 1, 23), 1000);
            final Audit f3 = new Audit(OS, LH, p3, new AuditState(AuditStateId.FINALIZED), LocalDate.now(), LocalDate.of(2017, 3, 25), 100);
            final Audit f4 = new Audit(LH, OS, p9, new AuditState(AuditStateId.FINALIZED), LocalDate.now(), LocalDate.of(2017, 2, 5), 1000);
            final Audit f5 = new Audit(OS, LH, p9, new AuditState(AuditStateId.FINALIZED), LocalDate.now(), LocalDate.of(2017, 2, 12), 100);
            final Audit f6 = new Audit(LH, OS, p8, new AuditState(AuditStateId.FINALIZED), LocalDate.now(), LocalDate.of(2017, 1, 10), 1000);

            auditRepo.save(Arrays.asList(a1, a2, a3, a4, a5, f1, f2, f3, f4, f5, f6));

            log.info("Audits found with findAll:");
            log.info("--------------------------------------------");
            for (final Audit audit : auditRepo.findAll()) {
                log.info(audit.toString());
            }
            log.info("");
        };
    }
}
