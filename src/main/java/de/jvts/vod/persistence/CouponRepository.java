package de.jvts.vod.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import de.jvts.vod.domainobject.Coupon;

public interface CouponRepository extends JpaRepository<Coupon, Long> {

	List<Coupon> findByFlownCarrier(String flownCarrier);

}
