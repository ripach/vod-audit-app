package de.jvts.vod.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

import de.jvts.vod.domainvalue.Carrier;

public interface CarrierRepository extends JpaRepository<Carrier, Long> {

}
