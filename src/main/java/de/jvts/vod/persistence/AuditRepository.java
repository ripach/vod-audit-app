package de.jvts.vod.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import de.jvts.vod.domainobject.Audit;

public interface AuditRepository extends JpaRepository<Audit, Long> {

    @Query("select a from  Audit a where a.state.carrierAssignment = 'ISSUING' and a.issuingCarrier.twoLetterCode = :carrierCode "
            + "or a.state.carrierAssignment = 'FLOWN' and a.flownCarrier.twoLetterCode = :carrierCode")
    List<Audit> findActiveOwnAudits(@Param("carrierCode") String carrierCode);

    @Query("select a from  Audit a where a.state.carrierAssignment = 'ISSUING' and a.flownCarrier.twoLetterCode = :carrierCode "
            + "or a.state.carrierAssignment = 'FLOWN' and a.issuingCarrier.twoLetterCode = :carrierCode")
    List<Audit> findActivePartnerAudits(@Param("carrierCode") String carrierCode);

    @Query("select a from Audit a where a.state.activity = 'FINALIZED'")
    List<Audit> findFinishedAudits();
}
